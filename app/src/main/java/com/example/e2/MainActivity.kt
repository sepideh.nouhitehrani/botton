package com.example.e2

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding

import androidx.compose.material3.Button
import androidx.compose.runtime.*
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import kotlin.random.Random

import com.example.e2.ui.theme.E2Theme

@Composable
fun  Greeting(s: String) {
    var state by remember {
        mutableStateOf(GardenState.EMPTY)
    }

    var earnings by remember {
        mutableStateOf(0)
    }

    Column {
        when (state) {
            GardenState.EMPTY -> Image(
                painter = painterResource(id = R.drawable.one),
                contentDescription = "Empty Garden",
                modifier = Modifier.weight(1f)
            )
            GardenState.PLANTS -> Image(
                painter = painterResource(id = R.drawable.two),
                contentDescription = "Plants in Garden",
                modifier = Modifier.weight(1f)
            )
            GardenState.FRUIT -> Image(
                painter = painterResource(id = R.drawable.three3),
                contentDescription = "Fruits in Garden",
                modifier = Modifier.weight(1f)
            )
        }

        Button(
            onClick = {
                when (state) {
                    GardenState.EMPTY -> {
                        state = GardenState.PLANTS
                    }
                    GardenState.PLANTS -> {
                        state = GardenState.FRUIT
                    }
                    GardenState.FRUIT -> {
                        val earningsToday = Random.nextInt(1, 6)
                        earnings += earningsToday
                        state = GardenState.EMPTY
                    }
                }
            },
            modifier = Modifier.padding(16.dp)
        ) /*{
            Text(text = "Press Me")
        }

        Text(
            text = "Today's Earnings: €$earnings",
            modifier = Modifier.padding(16.dp)
        )*/
    }
}

private fun Any.Button(onClick: () -> Unit, modifier: Modifier) {

}

enum class GardenState {
    EMPTY,
    PLANTS,
    FRUIT
}



@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    E2Theme {
        Greeting("Android")
    }
}


